/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.7.28 : Database - ansql
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ansql` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ansql`;

/*Table structure for table `shop_cart` */

DROP TABLE IF EXISTS `shop_cart`;

CREATE TABLE `shop_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL COMMENT '用户ID',
  `goodsID` int(11) NOT NULL COMMENT '商品ID',
  `goodsNum` smallint(6) DEFAULT '1' COMMENT '商品数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='购物车';

/*Table structure for table `shop_goods` */

DROP TABLE IF EXISTS `shop_goods`;

CREATE TABLE `shop_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '商品名称',
  `typeId` int(11) DEFAULT NULL COMMENT '商品类型',
  `price` decimal(7,2) DEFAULT NULL COMMENT '商品价格',
  `image` varchar(60) COLLATE utf8_bin DEFAULT NULL COMMENT '商品图片',
  `sellCount` int(11) DEFAULT NULL COMMENT '商品销售数量',
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间（自动生成）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='商品表';

/*Table structure for table `shop_goods_detail` */

DROP TABLE IF EXISTS `shop_goods_detail`;

CREATE TABLE `shop_goods_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goodsId` int(11) NOT NULL COMMENT '商品ID',
  `detail` text COLLATE utf8_bin COMMENT '商品详情',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='商品详情表';

/*Table structure for table `shop_goods_type` */

DROP TABLE IF EXISTS `shop_goods_type`;

CREATE TABLE `shop_goods_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` char(20) COLLATE utf8_bin NOT NULL COMMENT '分类名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='商品分类表';

/*Table structure for table `shop_order` */

DROP TABLE IF EXISTS `shop_order`;

CREATE TABLE `shop_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `userID` int(11) NOT NULL COMMENT '用户ID',
  `totalMoney` decimal(7,2) DEFAULT '0.00' COMMENT '订单总金额',
  `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  `state` int(11) DEFAULT '1' COMMENT '订单状态：1000待付款，1100待发货，1110待收货，1120待评价，2000交易完成',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='订单表';

/*Table structure for table `shop_order_detail` */

DROP TABLE IF EXISTS `shop_order_detail`;

CREATE TABLE `shop_order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderID` int(11) NOT NULL COMMENT '订单ID',
  `goodsID` int(11) NOT NULL COMMENT '商品ID',
  `goodsNum` smallint(6) DEFAULT '1' COMMENT '商品数量',
  `money` decimal(7,2) DEFAULT '0.00' COMMENT '商品单价',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='订单详情表';

/*Table structure for table `shop_user` */

DROP TABLE IF EXISTS `shop_user`;

CREATE TABLE `shop_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '用户名',
  `password` varchar(32) COLLATE utf8_bin NOT NULL COMMENT '密码',
  `email` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '邮箱',
  `phone` char(11) COLLATE utf8_bin DEFAULT NULL COMMENT '手机号',
  `grade` smallint(6) DEFAULT '2' COMMENT '用户等级： 1管理员 2普通用户',
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `balance` decimal(7,2) DEFAULT '0.00' COMMENT '总余额',
  `integral` smallint(6) DEFAULT '0' COMMENT '总积分',
  `userPhoto` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '用户头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户表';

/*Table structure for table `shop_user_action_log` */

DROP TABLE IF EXISTS `shop_user_action_log`;

CREATE TABLE `shop_user_action_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) DEFAULT NULL COMMENT '用户ID',
  `balance` decimal(7,2) DEFAULT '0.00' COMMENT '余额',
  `integral` smallint(6) DEFAULT '0' COMMENT '积分',
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户信息表，保存用户余额，积分等数据的修改记录';

/*Table structure for table `shop_user_address` */

DROP TABLE IF EXISTS `shop_user_address`;

CREATE TABLE `shop_user_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL COMMENT '用户ID',
  `consignee` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '联系人',
  `tel` char(12) COLLATE utf8_bin DEFAULT NULL COMMENT '联系电话',
  `address` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户收货地址表';

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
