package com.example.shop.modules.sys.service.impl;

import com.example.shop.persistence.entity.GoodsType;
import com.example.shop.persistence.mapper.GoodsTypeMapper;
import com.example.shop.modules.sys.service.IGoodsTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品分类表 服务实现类
 * </p>
 *
 * @author jason
 * @since 2019-12-16
 */
@Service
public class GoodsTypeServiceImpl extends ServiceImpl<GoodsTypeMapper, GoodsType> implements IGoodsTypeService {
    /*@Autowired
    GoodsTypeMapper goodsTypeMapper;

    public int insertGoodsType(GoodsType goodsType){
        return goodsTypeMapper.insert(goodsType);
    }

    public int updateGoodsType(GoodsType goodsType){
        return goodsTypeMapper.updateByPrimaryKey(goodsType);
    }

    public int deleteGoodsType(Integer id){
        return goodsTypeMapper.deleteByPrimaryKey(id);
    }*/
}
