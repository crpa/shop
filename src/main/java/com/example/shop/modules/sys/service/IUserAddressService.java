package com.example.shop.modules.sys.service;

import com.example.shop.persistence.entity.UserAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户收货地址表 服务类
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
public interface IUserAddressService extends IService<UserAddress> {

}
