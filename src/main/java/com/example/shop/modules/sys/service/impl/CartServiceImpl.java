package com.example.shop.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.shop.persistence.entity.Cart;
import com.example.shop.persistence.mapper.CartMapper;
import com.example.shop.modules.sys.service.ICartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.shop.utils.MPPageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 购物车 服务实现类
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
@Service
public class CartServiceImpl extends ServiceImpl<CartMapper, Cart> implements ICartService {

    @Autowired
    private CartMapper cartMapper;

    @Override
    public int deleteCarts(String ids){
        // 先转换为字符串数组
        String[] strs = ids.split(",");
        List<Long> ints = new LinkedList<Long>();
        for(String id: strs){
            // 在转换为long 类型数组
            ints.add(Long.valueOf(id));
        }
        //批量删除
        return cartMapper.deleteBatchIds(ints);
    }

    //分页查询
    @Override
    public IPage<Cart> selectListCart(Integer page, Integer pageSize, Integer userID){
        Page<Cart> pages = MPPageFactory.createPage(page,pageSize);
        QueryWrapper ew = new QueryWrapper();
        if (userID != null){
            ew.eq("userID",userID);
        }
        return pages.setRecords(cartMapper.selectListCart(pages,ew));
    }

}
