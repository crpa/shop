package com.example.shop.modules.sys.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.shop.persistence.entity.Goods;
import com.example.shop.persistence.entity.GoodsDetail;
import com.example.shop.modules.sys.service.IGoodsService;
import com.example.shop.utils.JSONResult;
import com.example.shop.utils.MPPageFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;

@Api(tags = "商品接口")
@RestController
@RequestMapping("/sys/goods")
public class GoodsController {

    @Autowired
    private IGoodsService goodsService;

    @ApiOperation(value = "查询商品详情",notes = "通过ID查商品详情")
    @ApiImplicitParam(name = "id", value = "商品ID", required = true, dataType = "Int", paramType = "path")
    @GetMapping("/{id}")
    public JSONResult getGoods(@PathVariable("id") Integer id){
        Goods goods = goodsService.selectGoodsInfoAll(id);
        return JSONResult.renderSuccess(goods);
    }

    /**
     * 查询商品列表
     * @param page
     * @param pageSize
     * @return List
     */
    @ApiOperation(value = "查询商品列表",notes = "分页查询加条件")
    @GetMapping("/list")
    public JSONResult selectGoodsList(Integer page, Integer pageSize, Integer typeId){
        // 分页数据
        Page<Goods> pages = MPPageFactory.createPage(page, pageSize);
        // 条件语句
        QueryWrapper<Goods> ew = new QueryWrapper<>();
        if(typeId != null){
            ew.eq("type_id", typeId);
        }
        //查询结果
        IPage<Goods> list = goodsService.selectGoodsList(pages,ew);
        return JSONResult.renderSuccess(list);
    }

    /**
     * 新增商品
     * @param goods, goodsDetail
     * @return List
     */
    @ApiOperation(value = "新增商品",notes = "新增商品详细信息，关联表新增")
    @PostMapping("/insert")
    public JSONResult insertGoodsAll(Goods goods, GoodsDetail goodsDetail){
        goodsService.insertGoodsAll(goods, goodsDetail);
        return JSONResult.renderSuccess();
    }

    /**
     * 删除商品
     * @param ids
     * @return
     */
    @ApiOperation(value = "删除商品",notes = "可批量删除")
    @PostMapping("/delete")
    public JSONResult deleteGoods(String ids){
        // 非空字符串判断
        if(StringUtils.isEmpty(ids)){
            return JSONResult.renderError();
        }
        // 字符串转数组
        String[] strArray =  ids.split(",");
        goodsService.deleteGoods(strArray);
        return JSONResult.renderSuccess();
    }

    /**
     * 更新商品信息
     * @param goods
     * @return
     */
    @ApiOperation(value = "更新商品信息",notes = "关联表更新")
    @PostMapping("/update")
    public JSONResult updateGoods(Goods goods){
        goodsService.updateGoodsAll(goods);
        return JSONResult.renderSuccess();
    }



}
