package com.example.shop.modules.sys.service.impl;

import com.example.shop.persistence.entity.UserActionLog;
import com.example.shop.persistence.mapper.UserActionLogMapper;
import com.example.shop.modules.sys.service.IUserActionLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息表，保存用户余额，积分等数据的修改记录 服务实现类
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
@Service
public class UserActionLogServiceImpl extends ServiceImpl<UserActionLogMapper, UserActionLog> implements IUserActionLogService {

}
