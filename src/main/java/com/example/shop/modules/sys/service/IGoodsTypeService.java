package com.example.shop.modules.sys.service;

import com.example.shop.persistence.entity.GoodsType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品分类表 服务类
 * </p>
 *
 * @author jason
 * @since 2019-12-16
 */
public interface IGoodsTypeService extends IService<GoodsType> {
    /*int insertGoodsType(GoodsType goodsType);

    int updateGoodsType(GoodsType goodsType);

    int deleteGoodsType(Integer id);*/
}
