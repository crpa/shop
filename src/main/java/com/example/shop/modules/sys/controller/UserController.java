package com.example.shop.modules.sys.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.shop.modules.sys.service.IUserService;
import com.example.shop.persistence.entity.User;
import com.example.shop.utils.JSONResult;
import com.example.shop.utils.MPPageFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
@Api(tags = "用户表相关接口")
@RestController
@RequestMapping("/sys/user")
public class UserController {
    @Autowired
    private IUserService userService;

    @ApiOperation(value = "用户注册")
    @PostMapping("/register")
    public JSONResult insertUser(User user){
        return JSONResult.renderSuccess(userService.save(user));
    }

    /**
     * 检查用户名和邮箱是否被占用
     * @param userName
     * @param email
     * @return num  0：没有   1有
     */
    @ApiOperation(value = "检查用户名和邮箱是否被占用")
    @PostMapping("/check")
    public JSONResult checkUser(String userName, String email){
        Integer num = 0;
        if(!StringUtils.isEmpty(userName)){
            QueryWrapper ew = new QueryWrapper();
            ew.eq("userName",userName);
            User user = userService.getOne(ew,false);
            if(!StringUtils.isEmpty(user)){
                num = 1;
            }
        }
        if(!StringUtils.isEmpty(email)){
            QueryWrapper ew = new QueryWrapper();
            ew.eq("email",email);
            User user = userService.getOne(ew,false);
            if(!StringUtils.isEmpty(user)){
                num = 1;
            }
        }
        return JSONResult.renderSuccess(num);
    }

    @ApiOperation(value = "修改用户信息")
    @PostMapping("/update")
    public JSONResult updateUser(User user){
        return JSONResult.renderSuccess(userService.updateById(user));
    }

    @ApiOperation(value = "删除用户")
    @GetMapping("/delete")
    public JSONResult deleteUser(Integer id){
        return JSONResult.renderSuccess(userService.removeById(id));
    }

    @ApiOperation(value = "用户列表", notes = "分页查询")
    @ApiImplicitParam(name="grade",value="用户等级： 1管理员 2普通用户")
    @GetMapping("/list")
    public JSONResult getUserList(Integer page, Integer pageSize, Integer grade){
        Page<User> pages = MPPageFactory.createPage(page,pageSize);
        QueryWrapper ew = new QueryWrapper();
        ew.eq("grade", grade);
        IPage<User> list;
        if(grade == null){
            list = userService.page(pages);
        }else{
            list = userService.page(pages,ew);
        }
        return JSONResult.renderSuccess(list);
    }

    @ApiOperation(value = "用户登录",notes = "登录成功返回userID")
    @PostMapping("/login")
    public JSONResult loginUser(String username, String password){
        return userService.loginUser(username,password);
    }

    @ApiOperation(value = "查询用户信息")
    @GetMapping("/{id}")
    public JSONResult getUserInfo(Integer id){
        return JSONResult.renderSuccess(userService.getById(id));
    }

}

