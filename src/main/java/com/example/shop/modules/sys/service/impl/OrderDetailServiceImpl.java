package com.example.shop.modules.sys.service.impl;

import com.example.shop.persistence.entity.OrderDetail;
import com.example.shop.persistence.mapper.OrderDetailMapper;
import com.example.shop.modules.sys.service.IOrderDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单详情表 服务实现类
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements IOrderDetailService {

}
