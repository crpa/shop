package com.example.shop.modules.sys.service;

import com.example.shop.persistence.entity.OrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单详情表 服务类
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
public interface IOrderDetailService extends IService<OrderDetail> {

}
