package com.example.shop.modules.sys.controller;


import cn.hutool.core.util.StrUtil;
import com.example.shop.modules.sys.service.IOrderDetailService;
import com.example.shop.modules.sys.service.IOrderService;
import com.example.shop.persistence.entity.Order;
import com.example.shop.utils.JSONResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
@Api(tags = "订单表")
@RestController
@RequestMapping("/sys/order")
public class OrderController {

    @Autowired
    private IOrderService orderService;

    @ApiOperation(value = "新增订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name="goodsJSON",value="商品JSON字符串： {[{goodsID:1,goodsNum:1,money:25}]}")})
    @PostMapping("/add")
    public JSONResult addOrder(Order order, String goodsJSON){
        orderService.addOrder(order,goodsJSON);
        return JSONResult.renderSuccess();
    }

    @ApiOperation(value = "修改订单(付款后修改状态）")
    @PostMapping("/update")
    public JSONResult updateOrderInfo(Order order){
        return JSONResult.renderSuccess(orderService.updateById(order));
    }

    @ApiOperation(value = "删除订单")
    @GetMapping("/delete")
    public JSONResult deleteOrderInfo(String ids){
        return JSONResult.renderSuccess( orderService.deleteOrderInfo(ids));
    }

    @ApiOperation(value = "分页查询",notes = "根据状态分页查询")
    @GetMapping("/list")
    public JSONResult getOrderInfoList(Integer page, Integer pageSize, Integer state, Integer userID){
        return JSONResult.renderSuccess(orderService.selectOrderInfoList(page,pageSize,state,userID));
    }

    @ApiOperation(value = "查询订单详情")
    @GetMapping("/{id}")
    public JSONResult getOrderInfo(Integer id){
        return JSONResult.renderSuccess(orderService.selectOrderInfo(id));
    }

}

