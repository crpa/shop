package com.example.shop.modules.sys.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.shop.persistence.entity.Goods;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.shop.persistence.entity.GoodsDetail;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author jason
 * @since 2019-12-16
 */
public interface IGoodsService extends IService<Goods> {
    // 查询
    Goods selectGoodsInfoAll(Integer id);

    // 分页查询
    IPage<Goods> selectGoodsList(Page<Goods> page, QueryWrapper<Goods> ew);

    //新增  关联表新增
    void insertGoodsAll(Goods goods, GoodsDetail goodsDetail);

    //删除 关联表删除
    void deleteGoods(String[] ids);

    //更新 关联表更新
    void updateGoodsAll(Goods goods);
}
