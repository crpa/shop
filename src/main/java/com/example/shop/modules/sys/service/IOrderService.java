package com.example.shop.modules.sys.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.shop.persistence.entity.Goods;
import com.example.shop.persistence.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
public interface IOrderService extends IService<Order> {
    // 新增订单   关联表插入
    void addOrder(Order order, String goodsJSON);

    // 删除订单
    int deleteOrderInfo(String ids);

    // 分页查询列表  关联表
    IPage<Order> selectOrderInfoList(Integer page, Integer pageSize, Integer state, Integer userID);

    // 订单详情
    Order selectOrderInfo(Integer id);

}
