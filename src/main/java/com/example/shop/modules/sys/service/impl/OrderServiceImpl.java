package com.example.shop.modules.sys.service.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.shop.persistence.entity.Goods;
import com.example.shop.persistence.entity.Order;
import com.example.shop.persistence.entity.OrderDetail;
import com.example.shop.persistence.mapper.OrderDetailMapper;
import com.example.shop.persistence.mapper.OrderMapper;
import com.example.shop.modules.sys.service.IOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.shop.utils.MPPageFactory;
import com.example.shop.utils.MpGenerator;
import org.apache.ibatis.annotations.Param;
import org.apache.velocity.util.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderDetailServiceImpl orderDetailService;

    /*  下单  */
    @Override
    public void addOrder(Order order, String goodsJSON){
        /*  订单表插入  */
        orderMapper.insert(order);
        if(StrUtil.isNotEmpty(goodsJSON)){
            /*  解析JSON  */
            List<OrderDetail> orderDetailList = JSON.parseArray(goodsJSON,OrderDetail.class);
            for(OrderDetail orderDetail: orderDetailList){
                /*  订单表插入 返回订单ID  保存至订单详情数据中 */
                orderDetail.setOrderID(order.getId());
            }
            // 订单详情表批量插入数据
            orderDetailService.saveBatch(orderDetailList);
        }
    }

    /*删除*/
    public int deleteOrderInfo(String ids){
        // 先转换为字符串数组
        String[] strs = ids.split(",");
        List<Long> ints = new LinkedList<Long>();
        for(String id: strs){
            // 在转换为long 类型数组
            ints.add(Long.valueOf(id));
            QueryWrapper ew = new QueryWrapper();
            ew.eq("orderID", id);
            // 先删除订单详情表中数据
            orderDetailService.remove(ew);
        }
        //批量删除
        return orderMapper.deleteBatchIds(ints);
    }

    /*分页查询全部信息*/
    public IPage<Order> selectOrderInfoList(Integer page, Integer pageSize, Integer state, Integer userID){
        Page<Order> pages = MPPageFactory.createPage(page,pageSize);
        QueryWrapper ew = new QueryWrapper();
        if(state != null){
            ew.eq("state", state);
        }
        if(userID != null){
            ew.eq("userID",userID);
        }
        // 返回分页类型 列表集合数据
        return pages.setRecords(orderMapper.selectOrderInfoList(pages,ew));
    }

    /*订单详情*/
    public Order selectOrderInfo(Integer id){
        //制造分页数据
        Page<Order> pages = MPPageFactory.createPage(1,1);
        QueryWrapper ew = new QueryWrapper();
        // 添加查询条件
        ew.eq("id",id);
        // 使用公共的分页查询方法
        List<Order> lists = orderMapper.selectOrderInfoList(pages,ew);
        // 新建order 对象
        Order order = new Order();
        if(!lists.isEmpty()){
            // 查询结果不为空的时候取第一条数据
            order = lists.get(0);
        }
        return order;
    }
}
