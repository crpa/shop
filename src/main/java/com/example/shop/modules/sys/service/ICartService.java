package com.example.shop.modules.sys.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.shop.persistence.entity.Cart;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 购物车 服务类
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
public interface ICartService extends IService<Cart> {

    // 批量删除
    int deleteCarts(String ids);

    IPage<Cart> selectListCart(Integer page, Integer pageSize, Integer userID);
}
