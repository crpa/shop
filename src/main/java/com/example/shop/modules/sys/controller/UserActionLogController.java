package com.example.shop.modules.sys.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 用户信息表，保存用户余额，积分等数据的修改记录 前端控制器
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
@Controller
@RequestMapping("/userActionLog")
public class UserActionLogController {

}

