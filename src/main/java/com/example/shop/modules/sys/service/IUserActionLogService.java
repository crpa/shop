package com.example.shop.modules.sys.service;

import com.example.shop.persistence.entity.UserActionLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户信息表，保存用户余额，积分等数据的修改记录 服务类
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
public interface IUserActionLogService extends IService<UserActionLog> {

}
