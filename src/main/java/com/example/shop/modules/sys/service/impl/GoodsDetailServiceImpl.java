package com.example.shop.modules.sys.service.impl;

import com.example.shop.persistence.entity.GoodsDetail;
import com.example.shop.persistence.mapper.GoodsDetailMapper;
import com.example.shop.modules.sys.service.IGoodsDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品详情表 服务实现类
 * </p>
 *
 * @author jason
 * @since 2019-12-16
 */
@Service
public class GoodsDetailServiceImpl extends ServiceImpl<GoodsDetailMapper, GoodsDetail> implements IGoodsDetailService {

}
