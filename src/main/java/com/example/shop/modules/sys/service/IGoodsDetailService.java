package com.example.shop.modules.sys.service;

import com.example.shop.persistence.entity.GoodsDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品详情表 服务类
 * </p>
 *
 * @author jason
 * @since 2019-12-16
 */
public interface IGoodsDetailService extends IService<GoodsDetail> {

}
