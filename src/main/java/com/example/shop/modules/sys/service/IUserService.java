package com.example.shop.modules.sys.service;

import com.example.shop.persistence.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.shop.utils.JSONResult;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
public interface IUserService extends IService<User> {

    // 用户登录
    JSONResult loginUser(String username, String password);
}
