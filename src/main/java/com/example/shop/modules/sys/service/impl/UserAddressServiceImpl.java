package com.example.shop.modules.sys.service.impl;

import com.example.shop.persistence.entity.UserAddress;
import com.example.shop.persistence.mapper.UserAddressMapper;
import com.example.shop.modules.sys.service.IUserAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户收货地址表 服务实现类
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
@Service
public class UserAddressServiceImpl extends ServiceImpl<UserAddressMapper, UserAddress> implements IUserAddressService {

}
