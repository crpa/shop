package com.example.shop.modules.sys.controller;


import cn.hutool.core.util.StrUtil;
import com.example.shop.modules.sys.service.ICartService;
import com.example.shop.persistence.entity.Cart;
import com.example.shop.utils.JSONResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 购物车 前端控制器
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
@Api(tags = "购物车")
@RestController
@RequestMapping("/sys/cart")
public class CartController {

    @Autowired
    private ICartService cartService;

    @ApiOperation(value = "新增或修改")
    @PostMapping("/add")
    public JSONResult addCart(Cart cart){
        return JSONResult.renderSuccess(cartService.saveOrUpdate(cart));
    }

    @ApiOperation(value = "删除")
    @GetMapping("/delete")
    public JSONResult deleteCart(String ids){
        return JSONResult.renderSuccess(cartService.deleteCarts(ids));
    }

    @ApiOperation(value = "查询", notes = "分页条件查询")
    @GetMapping("/list")
    public JSONResult selectListCart(Integer page, Integer pageSize, Integer userID){
        return JSONResult.renderSuccess(cartService.selectListCart(page,pageSize,userID));
    }
}

