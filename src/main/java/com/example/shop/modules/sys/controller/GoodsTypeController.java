package com.example.shop.modules.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.shop.modules.sys.service.IGoodsTypeService;
import com.example.shop.persistence.entity.GoodsType;
import com.example.shop.persistence.mapper.GoodsMapper;
import com.example.shop.persistence.mapper.GoodsTypeMapper;
import com.example.shop.utils.JSONResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.UpdateProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "商品分类接口")
@RestController
@RequestMapping("/sys/goodsType")
public class GoodsTypeController {
    @Autowired
    IGoodsTypeService goodsTypeService;

    @ApiOperation(value = "新增商品分类",notes = "")
    @PostMapping("/insert")
    public JSONResult insertGoodsType(GoodsType goodsType){
        return JSONResult.renderSuccess(goodsTypeService.save(goodsType));
    }

    @ApiOperation(value = "修改")
    @PostMapping("/update")
    public JSONResult updateGoodsType(GoodsType goodsType){
        return JSONResult.renderSuccess(goodsTypeService.updateById(goodsType));
    }

    @ApiOperation(value = "删除")
    @GetMapping("/delete/{id}")
    public JSONResult deleteGoodsType(@PathVariable("id") Integer id){
        return JSONResult.renderSuccess(goodsTypeService.removeById(id));
    }

    @ApiOperation(value = "查询详情")
    @GetMapping("/{id}")
    public JSONResult selectGoodsType(@PathVariable("id") Integer id){
        GoodsType goodsType = goodsTypeService.getById(id);
        return JSONResult.renderSuccess(goodsType);
    }

    @ApiOperation(value = "查询列表",notes = "查询商品全部分类，不分页")
    @GetMapping("/list")
    public JSONResult getGoodsTypeList(){
        List<GoodsType> list = goodsTypeService.list();
        return JSONResult.renderSuccess(list);
    }

}
