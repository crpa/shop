package com.example.shop.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.shop.persistence.entity.User;
import com.example.shop.persistence.mapper.UserMapper;
import com.example.shop.modules.sys.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.shop.utils.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private UserMapper userMapper;


    /*
    用户登录
     */
    @Override
    public JSONResult loginUser(String username, String password){
        QueryWrapper ew = new QueryWrapper();
        ew.eq("username",username);
        User user = userMapper.selectOne(ew);
        if(user.getPassword().equals(password)){
            return JSONResult.renderSuccess(user.getId());
        }
        return JSONResult.renderError();
    }
}
