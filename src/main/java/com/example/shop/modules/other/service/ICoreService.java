package com.example.shop.modules.other.service;

import com.google.zxing.WriterException;

import java.io.IOException;
import java.util.Map;

public interface ICoreService {

    Map<String, Object> createCore(String text, Integer width, Integer height)throws WriterException, IOException;

}
