package com.example.shop.modules.other.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface IUploadImgService {

    Map<String, Object> uploadImg(MultipartFile file);

}
