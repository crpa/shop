package com.example.shop.modules.other.controller;

import com.example.shop.modules.other.service.ICoreService;
import com.example.shop.modules.other.service.IUploadImgService;
import com.example.shop.utils.JSONResult;
import com.example.shop.utils.QRCodeUtils;
import com.google.zxing.WriterException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * <p>
 * 其他接口 前端控制器
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
@Api(tags = "其他接口")
@RestController
@RequestMapping("/an")
public class OtherController {

    @Autowired
    private IUploadImgService uploadImgService;
    @Autowired
    private ICoreService coreService;


    @ApiOperation(value = "上传图片")
    @PostMapping("/upload/img")
    public JSONResult uploadImg(@RequestParam(value="file",required=false)MultipartFile file){
        return JSONResult.renderSuccess(uploadImgService.uploadImg(file));
    }

    @ApiOperation(value = "生成二维码")
    @PostMapping("/core")
    public JSONResult getCore(String text, Integer width, Integer height, String logoPath)throws Exception{
        //return JSONResult.renderSuccess(coreService.createCore(text,width,height));
        return JSONResult.renderSuccess(QRCodeUtils.encode(text, logoPath, "", true));
    }


}
