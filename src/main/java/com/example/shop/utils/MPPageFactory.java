package com.example.shop.utils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @Author：LJ
 * @Description：
 * @Date: 2018/3/27
 * @Modified By:
 */
public class MPPageFactory {

    public static Page createPage(Integer page, Integer pageSize) {
        page = page != null ? page : 1;
        pageSize = pageSize != null ? pageSize : 10;
        Page pagination = new Page(page, pageSize);
        //不进行count sql优化，解决MP无法自动优化SQL问题
        pagination.setOptimizeCountSql(false);
        //查询总记录数
        pagination.setSearchCount(true);
        return pagination;
    }
}
