package com.example.shop.utils;

/**
 * @Description
 * @Author by sm
 * @Create 2017/05/09 11:42
 */

public class JSONResult {
    public final static Integer SUCCESS=1;
    public final static Integer ERROR=0;

    private Integer code;
    private String msg;
    private Object result;

    public JSONResult() {
    }

    public JSONResult(Integer code, String msg, Object result) {
        this.code = code;
        this.msg = msg;
        this.result = result;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    /**
     * 渲染失败数据
     *
     * @return result
     */
    public static JSONResult renderError() {
        JSONResult result = new JSONResult();
        result.setCode(ERROR);
        result.setMsg("error");
        return result;
    }

    /**
     * 渲染失败数据（带消息）
     *
     * @param msg 需要返回的消息
     * @return result
     */
    public static JSONResult renderError(String msg) {
        JSONResult result = renderError();
        result.setMsg(msg);
        return result;
    }

    /**
     * 渲染失败数据（带消息，带数据）
     *
     * @param msg 需要返回的消息
     * @param obj 需要返回的对象
     * @return result
     */
    public static JSONResult renderError(String msg, Object obj) {
        JSONResult result = renderError();
        result.setMsg(msg);
        result.setResult(obj);
        return result;
    }

    public static JSONResult renderError(Integer code, String msg, Object obj) {
        JSONResult result = renderError();
        result.setCode(code);
        result.setMsg(msg);
        result.setResult(obj);
        return result;
    }

    /**
     * 渲染成功数据
     *
     * @return result
     */
    public static JSONResult renderSuccess() {
        JSONResult result = new JSONResult();
        result.setMsg("success");
        result.setCode(SUCCESS);
        return result;
    }

    /**
     * 渲染成功数据（带消息，带数据）
     *
     * @param obj 需要返回的对象
     * @return result
     */
    public static JSONResult renderSuccess(Object obj) {
        JSONResult result = renderSuccess();
        result.setResult(obj);
        return result;
    }
}
