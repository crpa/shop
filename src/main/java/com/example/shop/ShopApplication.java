package com.example.shop;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

import static cn.hutool.core.date.DatePattern.NORM_DATETIME_MINUTE_PATTERN;

@SpringBootApplication
public class ShopApplication extends WebMvcConfigurerAdapter {

	public static void main(String[] args) {
		SpringApplication.run(ShopApplication.class, args);
	}

    /**
     * @Description 配置fastJSON转换器
     * @Author LJ
     * @Date 2019/2/21 10:30
     */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        // 初始化转换器
        FastJsonHttpMessageConverter fastConvert = new FastJsonHttpMessageConverter();
        fastConvert.setFeatures(
                SerializerFeature.WriteDateUseDateFormat,
                SerializerFeature.WriteNullListAsEmpty,
                SerializerFeature.WriteNullStringAsEmpty,
                SerializerFeature.WriteMapNullValue,
                SerializerFeature.BrowserCompatible,
                SerializerFeature.DisableCircularReferenceDetect);
        converters.add(fastConvert);
        //这里设置FastJSON的默认时间格式为“yyyy-MM-dd HH:mm”
        JSON.DEFFAULT_DATE_FORMAT=NORM_DATETIME_MINUTE_PATTERN;
    }

}

