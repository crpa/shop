package com.example.shop.persistence.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 订单详情表
 * </p>
 *
 * @author jason
 * @since 2019-12-18
 */
@Data
@TableName("shop_order_detail")
public class OrderDetail extends Model<OrderDetail> {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 订单ID
     */
    @TableField("orderID")
    private Integer orderID;

    /**
     * 商品ID
     */
    @TableField("goodsID")
    private Integer goodsID;

    /**
     * 商品数量
     */
    @TableField("goodsNum")
    private Integer goodsNum;

    /**
     * 下单时商品单价
     */
    private BigDecimal money;

    /**
     * 商品表
     */
    @ApiModelProperty(hidden=true)
    @TableField(exist = false)
    private Goods goods;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderID() {
        return orderID;
    }

    public void setOrderID(Integer orderID) {
        this.orderID = orderID;
    }

    public Integer getGoodsID() {
        return goodsID;
    }

    public void setGoodsID(Integer goodsID) {
        this.goodsID = goodsID;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OrderDetail{" +
        "id=" + id +
        ", orderID=" + orderID +
        ", goodsID=" + goodsID +
        ", goodsNum=" + goodsNum +
        ", money=" + money +
        "}";
    }
}
