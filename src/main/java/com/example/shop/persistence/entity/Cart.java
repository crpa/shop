package com.example.shop.persistence.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.example.shop.config.swagger.IgnoreSwaggerParameter;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 购物车
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
@Data
@TableName("shop_cart")
public class Cart extends Model<Cart> {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID
     */
    @TableField("userID")
    private Integer userID;

    /**
     * 商品ID
     */
    @TableField("goodsID")
    private Integer goodsID;

    /**
     * 商品数
     */
    @TableField("goodsNum")
    private Integer goodsNum;

    /**
     * 商品表
     */
    @IgnoreSwaggerParameter
    @TableField(exist = false)
    private Goods goods;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public Integer getGoodsID() {
        return goodsID;
    }

    public void setGoodsID(Integer goodsID) {
        this.goodsID = goodsID;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Cart{" +
        "id=" + id +
        ", userID=" + userID +
        ", goodsID=" + goodsID +
        ", goodsNum=" + goodsNum +
        "}";
    }
}
