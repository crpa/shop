package com.example.shop.persistence.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author jason
 * @since 2019-12-16
 */
@Data
@ApiModel(value="goods对象",description="商品表goods")
@TableName("shop_goods")
public class Goods extends Model<Goods> {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value="商品ID",name="id",example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品名称
     */
    @ApiModelProperty(value="商品名称",required = true)
    private String name;

    /**
     * 商品类型
     */
    @ApiModelProperty(value="商品分类ID",example = "1")
    @TableField("typeId")
    private Integer typeId;

    /**
     * 商品价格
     */
    private BigDecimal price;

    /**
     * 商品图片
     */
    private String image;

    /**
     * 商品销售数量
     */
    @TableField("sellCount")
    private Integer sellCount;

    /**
     * 创建时间（自动生成）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @TableField("createTime")
    private LocalDateTime createTime;

    /**
     * 商品详情  在详情表中
     */
    @TableField(exist = false)
    @ApiModelProperty(value="商品详情")
    private String detail;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getSellCount() {
        return sellCount;
    }

    public void setSellCount(Integer sellCount) {
        this.sellCount = sellCount;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "id=" + id +
                ", name=" + name +
                ", typeId=" + typeId +
                ", price=" + price +
                ", image=" + image +
                ", sellCount=" + sellCount +
                ", createTime=" + createTime +
                "}";
    }
}
