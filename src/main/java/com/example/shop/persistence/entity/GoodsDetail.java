package com.example.shop.persistence.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * <p>
 * 商品详情表
 * </p>
 *
 * @author jason
 * @since 2019-12-16
 */
@ApiModel(value="goodsDetail对象",description="商品详情表")
@TableName("shop_goods_detail")
public class GoodsDetail extends Model<GoodsDetail> {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value="ID",name="id", hidden=true)
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品ID
     */
    @ApiModelProperty(value="商品ID",name="goodsId", hidden=true)
    @TableField("goodsId")
    private Integer goodsId;

    /**
     * 商品详情
     */
    private String detail;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "GoodsDetail{" +
                "id=" + id +
                ", goodsId=" + goodsId +
                ", detail=" + detail +
                "}";
    }
}
