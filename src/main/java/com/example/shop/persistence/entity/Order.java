package com.example.shop.persistence.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.example.shop.config.swagger.IgnoreSwaggerParameter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author jason
 * @since 2019-12-18
 */
@Data
@TableName("shop_order")
public class Order extends Model<Order> {

    private static final long serialVersionUID=1L;

    /**
     * 订单ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID
     */
    @TableField("userID")
    private Integer userID;

    /**
     * 订单总金额
     */
    @TableField("totalMoney")
    private BigDecimal totalMoney;

    /**
     * 下单时间
     */
    @TableField("createTime")
    private LocalDateTime createTime;

    /**
     * 订单状态：1000待付款，1100待发货，1110待收货，1120待评价，2000交易完成
     */
    @ApiModelProperty(value="千位\n" +
            "     -4000：交易失败\n" +
            "     -3000：系统取消\n" +
            "     -2000：卖家取消\n" +
            "     -1000：买家取消\n" +
            "     1000：创建，初始化状态\n" +
            "     2000：交易完成\n" +
            "     百位\n" +
            "     100：已支付\n" +
            "     十位\n" +
            "     10：已发货\n" +
            "     20:确认收货\n" +
            "     个位\n" +
            "     1：买家已评论\n" +
            "     2：卖家已评论\n" +
            "     3：双方已评论")
    private Integer state;

    @IgnoreSwaggerParameter
    @TableField(exist = false)
    private List<OrderDetail> orderDetails;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Order{" +
        "id=" + id +
        ", userID=" + userID +
        ", totalMoney=" + totalMoney +
        ", createTime=" + createTime +
        ", state=" + state +
        "}";
    }
}
