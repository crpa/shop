package com.example.shop.persistence.mapper;

import com.example.shop.persistence.entity.GoodsType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品分类表 Mapper 接口
 * </p>
 *
 * @author jason
 * @since 2019-12-16
 */
public interface GoodsTypeMapper extends BaseMapper<GoodsType> {

}
