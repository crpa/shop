package com.example.shop.persistence.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.shop.persistence.entity.Cart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.shop.persistence.entity.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 购物车 Mapper 接口
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
public interface CartMapper extends BaseMapper<Cart> {

    // 分页查询列表  关联表
    List<Cart> selectListCart(Page<Cart> page, @Param("ew") QueryWrapper<Cart> ew);
}
