package com.example.shop.persistence.mapper;

import com.example.shop.persistence.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
public interface UserMapper extends BaseMapper<User> {

}
