package com.example.shop.persistence.mapper;

import com.example.shop.persistence.entity.UserActionLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户信息表，保存用户余额，积分等数据的修改记录 Mapper 接口
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
public interface UserActionLogMapper extends BaseMapper<UserActionLog> {

}
