package com.example.shop.persistence.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.shop.persistence.entity.Goods;
import com.example.shop.persistence.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author jason
 * @since 2019-12-18
 */
public interface OrderMapper extends BaseMapper<Order> {
    // 分页查询列表  关联表
    List<Order> selectOrderInfoList(Page<Order> page, @Param("ew") QueryWrapper<Order> ew);

}
