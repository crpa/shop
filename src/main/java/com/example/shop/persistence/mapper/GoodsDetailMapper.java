package com.example.shop.persistence.mapper;

import com.example.shop.persistence.entity.GoodsDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品详情表 Mapper 接口
 * </p>
 *
 * @author jason
 * @since 2019-12-16
 */
public interface GoodsDetailMapper extends BaseMapper<GoodsDetail> {
    // 根据商品ID删除
    int deleteByIds(String[] goodsIds);

    // 更新商品详情
    int updateByGoodsId(GoodsDetail record);
}
