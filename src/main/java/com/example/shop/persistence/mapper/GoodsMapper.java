package com.example.shop.persistence.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.shop.persistence.entity.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author jason
 * @since 2019-12-16
 */
public interface GoodsMapper extends BaseMapper<Goods> {
    /*  关联表查询 查询商品全部信息  */
    Goods selectGoodsInfoAll(@Param("ew") QueryWrapper<Goods> ew);

    List<Goods> selectGoodsAllList(Page<Goods> page, @Param("ew") QueryWrapper<Goods> ew);

    /*  批量删除  */
    void deleteByIds(String[] ids);
}
