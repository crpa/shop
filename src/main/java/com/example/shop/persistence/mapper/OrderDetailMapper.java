package com.example.shop.persistence.mapper;

import com.example.shop.persistence.entity.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 订单详情表 Mapper 接口
 * </p>
 *
 * @author jason
 * @since 2019-12-18
 */
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {

    List<OrderDetail> getSelectList(Integer orderID);
}
