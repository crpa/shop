package com.example.shop.persistence.mapper;

import com.example.shop.persistence.entity.UserAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户收货地址表 Mapper 接口
 * </p>
 *
 * @author jason
 * @since 2019-12-17
 */
public interface UserAddressMapper extends BaseMapper<UserAddress> {

}
