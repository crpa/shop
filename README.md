# shop

#### 介绍
java商城后台：spring-boot框架搭载mybatis插件，swagger2生成动态接口文档。（开发中……）

#### 软件架构
~~~
├── src                             // 源代码
│   ├── main
│   │     ├── java
│   │     │      └── com.example.shop
│   │     │             ├── config                          // 相关插件配置
│   │     │             ├── modules.sys
│   │     │             │      ├── controller               // 控制层
│   │     │             │      └── service                  // 服务层
│   │     │             ├── persistence                     // 代码生成器生成文件
│   │     │             ├── utils
│   │     │             └── ShopApplication.java            // 启动器
│   │     └── resources
│   │             ├── data                                   // 数据库结构备份
│   │             ├── static                                 // 静态文件
│   │             └── templates                             // 模板文件
│     └── test                      // 权限管理
└── pom.xml                         // 项目依赖包
~~~
#### 安装教程
修改application.yml配置文件中数据库连接地址和密码。

#### 使用说明
接口文档地址：http://localhost:8011/swagger-ui.html


